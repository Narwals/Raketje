﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorSpawner : MonoBehaviour {
    [SerializeField] private GameObject m_meteor;  
    [SerializeField] private GameObject m_rocket;
    [SerializeField] private float m_spawnDelay;
    int counter;
    private void Update()
    {
        counter++;
        if (counter > m_spawnDelay)
        {
            counter = 0;
            Instantiate(m_meteor, new Vector3(m_rocket.transform.position.x, m_rocket.transform.position.y + 50, m_rocket.transform.position.z), new Quaternion(0, 0, 0, 0));
        }
    }
}
