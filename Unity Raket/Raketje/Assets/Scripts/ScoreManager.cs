﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreManager : MonoBehaviour
{
    private Text text;
    [SerializeField] private GameObject rocket;
    private void Start()
    {
        text = GetComponent<Text>();
    }

    private void Update()
    {
        text.text = rocket.transform.position.y.ToString();
    }

}
