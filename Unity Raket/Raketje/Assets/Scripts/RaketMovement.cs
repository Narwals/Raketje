﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaketMovement : MonoBehaviour
{
    private float RocketX;
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D rigidbody;

    void Update()
    {
        this.transform.position = new Vector3(RocketX, this.transform.position.y, 0);
        if (Input.GetKey(KeyCode.W))
        {
            rigidbody.AddForce(Vector3.up * speed);
        }

        if (Input.GetKey(KeyCode.A))
        {
            RocketX -= 1;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            RocketX += 1;
        }

        Camera.main.transform.position = new Vector3(0, this.transform.position.y, -50);

    }
}
